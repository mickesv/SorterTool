import java.util.Random;

public class Main {
	public static int [] generateArray() {
		Random r = new Random();
		int len = 5+r.nextInt(10);
		
		int [] arr = new int[len];
		for(int i=0; i < len; i++) {
			arr[i] = r.nextInt(1000);
		}

		return arr;
	}


	public static void printArray(int [] arr) {
		System.out.print("[");
		for (int i : arr) {
			System.out.print(i + ", ");
		}
		System.out.println("]");
	}

	public static void main(String [] args) {
		Sorter s = new Sorter(Sorter.SortAlgorithm.QuickSort);
		int [] arr = generateArray();

		printArray(s.sort(arr));
		printArray(s.quickSort(arr));
		printArray(s.insertionSort(arr));
		printArray(s.bubbleSort(arr));
		printArray(arr);
		
	}
}
