public class Sorter { 
	public enum SortAlgorithm {
		BubbleSort,
		QuickSort,
		InsertionSort;
	}

	private SortAlgorithm defaultAlgorithm;

	public Sorter() {
		this(SortAlgorithm.BubbleSort);
	}

	public Sorter(SortAlgorithm defaultAlgorithm) {
		this.defaultAlgorithm = defaultAlgorithm;
	}

	public int[] sort(int[] inputData) {
		switch (this.defaultAlgorithm) {
		case QuickSort: return quickSort(inputData); 
		case InsertionSort: return insertionSort(inputData); 
		case BubbleSort: 
		default: return bubbleSort(inputData); 
				
		}
	}

	public int[] bubbleSort(int[] inputData) {
		int[] out = inputData.clone();

		for(int outer = 0; outer < out.length; outer++) {
			for (int inner = outer+1; inner < out.length; inner++) {
				if (out[outer] < out[inner]) {
					int tmp = out[outer];
					out[outer] = out[inner];
					out[inner] = tmp;
				}
			}
		}

		return out;
	}

	public int[] insertionSort(int[] inputData) {
		int[] out = inputData.clone();

		for(int pos = 1; pos < out.length; pos++) {
			int key = out[pos];
			int iter = pos-1;

			while (iter >= 0 && key < out[iter]) {
				out[iter+1] = out[iter];
				--iter;
			}

			out[iter+1] = key;
		}

		return out;
	}

	private int partition(int[] arr, int leftIdx, int rightIdx) {
		int pivot = arr[Math.floorDiv((leftIdx + rightIdx), 2)]; // Pick an element somewhere in the middle

		while (leftIdx <= rightIdx) {
			// "sneak up" on the middle from both ends; keep going
			// as long as the elements are already smaller/bigger than the pivot
			while (arr[leftIdx] < pivot) { leftIdx++; }
			while (arr[rightIdx] > pivot) { rightIdx--; }

			// If they're not, and we still have elements to go
			// swap the two values that are out of order and keep going
			if (leftIdx <= rightIdx) {
				int tmp = arr[leftIdx];
				arr[leftIdx] = arr[rightIdx];
				arr[rightIdx] = tmp;

				leftIdx++;
				rightIdx--;
			}
		}

		return leftIdx; // This is the final pivot point
	}

	private int[] quickSort(int[] arr, int start, int end) {
		if (start == end) {
			return arr;
		}

		int index = partition(arr, start, end);
		if (start < index-1) {
			quickSort(arr, start, index-1);
		}
		if (index < end) {
			quickSort(arr, index, end);
		}

		return arr;
	}

	public int[] quickSort(int[] inputData) {
		int[] out = inputData.clone();
		return quickSort(out, 0, out.length-1);
	}
}
