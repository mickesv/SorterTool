import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;
import java.util.Arrays;

public class SorterTest {
    public static int [] generateArray(int minLength, int maxLength, int range) {
        Random r = new Random();
        int len = minLength+r.nextInt(maxLength-minLength);

        int [] arr = new int[len];
        for(int i=0; i < len; i++) {
            arr[i] = r.nextInt(range);
        }

        return arr;
    }

    public int[] sortArray(int [] input) {
        int [] out = input.clone();
        Arrays.sort(out);
        return out;
    }

    private int[] testArray;
    private int[] sortedArray;
    private Sorter srt;

    @BeforeEach
    void setUp() {
        testArray = generateArray(10,100,1000);
        sortedArray = sortArray(testArray);
        srt = new Sorter();
    }

    @Test
    void sort() {
        int [] out = srt.sort(testArray);
        assertEquals(out.length, testArray.length);
        assertTrue(isOrdered(out));
    }

    boolean isOrdered(int [] input) {
        for(int i = 0; i < input.length-1; i++) {
            if (input[i] > input [i+1]) return false;
        }
        return true;
    }

    @Test
    void bubbleSort() {
        int [] out = srt.bubbleSort(testArray);
        assertEquals(out.length, testArray.length);
        assertTrue(isOrdered(out));
    }

    @Test
    void insertionSort() {
        int [] out = srt.insertionSort(testArray);
        assertEquals(out.length, testArray.length);
        assertTrue(isOrdered(out));
    }

    @Test
    void quickSort() {
        int [] out = srt.quickSort(testArray);
        assertEquals(out.length, testArray.length);
        assertTrue(isOrdered(out));
    }
}
